function slider(){
  var windHeight = $(window).height();
  $('img').height(windHeight/1.2);

  var projet  = $('.projet');

  projet.each(function(){

    var slider  = $(this).children('.slider');
    var total = slider.children().length;
    var nb = 0;

    if ($(this).children(".legende").children('iframe').length > 0){
      var video = $(this).children().children('iframe');
      $(this).children(".slider").append('<div class="video"></div>');
      $(this).children(".slider").children('.video').prepend('<div class="btn"></div>');
      $(this).children(".slider").children('.video').append(video);
      $(this).children(".slider").children('.video').addClass(''+total);
    };

    var total2 = slider.children().length;



    slider.children().hide();
    slider.children('.0').show();

    slider.children().click(function(){

      if(nb == total2-1){
        nb = 0;
      } else {
        nb=nb+1;
      }

      var src = $(this).next('img').attr('data-original');
      $(this).next('img').attr('src', src);

      $(this).hide();
      slider.children('.'+nb).show();

      //var windowWidth = $(window).width();
      //var imageWidth = slider.children('.'+nb).width();
      //var marginleft = (windowWidth-imageWidth)/2;

      //slider.children('.'+nb).css({'margin-left': marginleft});
      //slider.children('.video').css({'margin-left': 0});

    })

  })

}

slider();
